package my.gov.jakim.mykad

import java.nio.file._

/**
 * Created with IntelliJ IDEA.
 * User: Mohd Solehuddin Abd Wahab
 * Date: 7/4/13
 * Time: 3:00 PM
 */
object FolderWatch {

  def watchdir(card: MyKadReaderScala,
               exeLocation: String,
               watchedLocation: String): Option[MyKadData] = {

    var data: Option[MyKadData] = null

    try {

      println("inside folder watcher")

      val watchedPath = Paths.get(watchedLocation)

      val watchService = FileSystems.getDefault.newWatchService
      watchedPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE)

      callExe(exeLocation)

      val watchKey = watchService.take
      val event = watchKey.pollEvents().get(0)
      val flagPath = event.context.asInstanceOf[Path]
      val exactPath = watchedPath.resolve(flagPath)

      data = if (flagPath.toString.equalsIgnoreCase("pass")) {
        println("proceed reading")
        Some(card.read())

      } else {
        println("Failed verification")
        None
      }
      Files.deleteIfExists(exactPath)
      watchKey.reset


    } catch {
      case e: Exception => println("Exception while in folder watching routine")
    }

    data //return data
  }

  def callExe(biometricExe: String) {
    println("calling " + biometricExe)
    val process = new ProcessBuilder("cmd","/C", "start", biometricExe).start()
  }

}
