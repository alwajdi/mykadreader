package my.gov.jakim.mykad

import javax.smartcardio.{Card, CommandAPDU, TerminalFactory}
import javax.xml.bind.DatatypeConverter
import java.io.ByteArrayOutputStream

/**
 * Created with IntelliJ IDEA.
 * User: Mohd Solehuddin Abd Wahab
 * Date: 6/30/13
 * Time: 5:50 AM
 */
class MyKadReaderScala {

  val SET_LENGTH: String = "C832000005080000"
  val SELECT: String = "CC00000008"
  val READ: String = "CC060000"
  val JPN_1_1: String = "01000100"
  val JPN_1_2: String = "02000100"
  val JPN_1_4: String = "04000100"
  val PADDING: String = "00"

  val dataSetJpn1_1: Map[String, Map[String, String]] = Map(
    MyKadReaderScala.NAME -> Map("LENGTH" -> "28", "OFFSET" -> "E900"),
    MyKadReaderScala.IC_NO -> Map("LENGTH" -> "0D", "OFFSET" -> "1101"),
    MyKadReaderScala.GENDER -> Map("LENGTH" -> "01", "OFFSET" -> "1E01"),
    MyKadReaderScala.OLD_IC -> Map("LENGTH" -> "08", "OFFSET" -> "1F01"),
    MyKadReaderScala.DOB -> Map("LENGTH" -> "04", "OFFSET" -> "2701"),
    MyKadReaderScala.BIRTH_PLACE -> Map("LENGTH" -> "19", "OFFSET" -> "2B01"),
    MyKadReaderScala.NATIONALITY -> Map("LENGTH" -> "12", "OFFSET" -> "4801"),
    MyKadReaderScala.RACE -> Map("LENGTH" -> "19", "OFFSET" -> "5A01"),
    MyKadReaderScala.RELIGION -> Map("LENGTH" -> "0B", "OFFSET" -> "7301"),
    MyKadReaderScala.DATE_ISSUED -> Map("LENGTH" -> "04", "OFFSET" -> "4401")
  )

  val dataSetJpn1_4: Map[String, Map[String, String]] = Map(
    MyKadReaderScala.ADDRESS_1 -> Map("LENGTH" -> "1E", "OFFSET" -> "0300"),
    MyKadReaderScala.ADDRESS_2 -> Map("LENGTH" -> "1E", "OFFSET" -> "2100"),
    MyKadReaderScala.ADDRESS_3 -> Map("LENGTH" -> "1E", "OFFSET" -> "3F00"),
    MyKadReaderScala.POSTCODE -> Map("LENGTH" -> "03", "OFFSET" -> "5D00"),
    MyKadReaderScala.CITY -> Map("LENGTH" -> "19", "OFFSET" -> "6000"),
    MyKadReaderScala.STATE -> Map("LENGTH" -> "1E", "OFFSET" -> "7900")
  )


  def read(): MyKadData = {
    val card = TerminalFactory.getDefault.terminals.list.get(0).connect("T=0")
    ceremony(card)
    val jpnResult1_1 = dataSetJpn1_1.map(data => (data._1, retrieveData(JPN_1_1, card, data._2).getBytes.dropRight(2)))
    val jpnResult1_4 = dataSetJpn1_4.map(data => (data._1, retrieveData(JPN_1_4, card, data._2).getBytes.dropRight(2)))

    val photoByteArray = retrievePhoto(JPN_1_2, card)

    MyKadData(jpnResult1_1, jpnResult1_4, photoByteArray)
  }

  def ceremony(card: Card) = {
    val selectJPN: Array[Byte] = DatatypeConverter.parseHexBinary("00A404000AA0000000744A504E0010")
    val selectGetResponse: Array[Byte] = DatatypeConverter.parseHexBinary("00C0000005")
    val selectJpnApdu: CommandAPDU = new CommandAPDU(selectJPN)
    val selectResponseApdu: CommandAPDU = new CommandAPDU(selectGetResponse)
    card.getBasicChannel.transmit(selectJpnApdu)
    card.getBasicChannel.transmit(selectResponseApdu)
  }

  def retrieveData(subset: String, card: Card, data: Map[String, String]) = {
    card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(SET_LENGTH + data("LENGTH") + PADDING)))
    card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(SELECT + subset + data("OFFSET") + data("LENGTH") + PADDING)))
    card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(READ + data("LENGTH"))))
  }

  def retrievePhoto(subset: String, card: Card) = {
    val byteArrayOutputStream = new ByteArrayOutputStream()

    //data length for photo is 4000. divided by maximum number that can be used in a request to the card is 255 or 0xFF is about 16 calls that needs to be made
    //on the 16th call, the requested data length should be 4000-(255*16) which is 175 or 0xAF

    var length = 0
    var offset = 0x03

    val responseApduVector = (1 to 16).map(count => {

      if (count < 16) length = 0xFF else length = 0xAF

      var tmpOffset = Util.toHexStr(offset)

      //if length of the offset value in String is 2, append 00. Else, rearrange the String representation so that the first letter prepended with 0 and then moved towards the back
      if (tmpOffset.length == 2) tmpOffset += "00" else tmpOffset = tmpOffset.tail + "0" + tmpOffset.head

      card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(SET_LENGTH + Util.toHexStr(length) + PADDING)))
      card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(SELECT + subset + tmpOffset + Util.toHexStr(length) + PADDING)))
      offset += length
      card.getBasicChannel.transmit(new CommandAPDU(DatatypeConverter.parseHexBinary(READ + Util.toHexStr(length))))
    })

    responseApduVector.map(responseApdu => byteArrayOutputStream.write(responseApdu.getData))
    byteArrayOutputStream
  }

}

object MyKadReaderScala {

  val NAME = "name"
  val IC_NO = "icNo"
  val GENDER = "gender"
  val OLD_IC = "oldIc"
  val DOB = "dob"
  val BIRTH_PLACE = "pob"
  val NATIONALITY = "nationality"
  val RACE = "race"
  val RELIGION = "religion"
  val ADDRESS_1 = "address1"
  val ADDRESS_2 = "address2"
  val ADDRESS_3 = "address3"
  val POSTCODE = "postcode"
  val CITY = "city"
  val STATE = "state"
  val DATE_ISSUED = "dateIssued"
  val PHOTO = "photo"

}
