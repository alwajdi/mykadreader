package my.gov.jakim.mykad

import scala.beans.BeanProperty
import java.io.ByteArrayOutputStream


/**
 * Created with IntelliJ IDEA.
 * User: Mohd Solehuddin Abd Wahab
 * Date: 6/30/13
 * Time: 8:53 AM
 */
class MyKadData(myKadInfoJpn1: Map[String, Array[Byte]],
                myKadInfoJpn2: Map[String, Array[Byte]],
                val photoByteArray: ByteArrayOutputStream ) {

  //jpn1
  @BeanProperty val name = new String(myKadInfoJpn1.get(MyKadReaderScala.NAME).get)
  @BeanProperty val icNo = new String(myKadInfoJpn1.get(MyKadReaderScala.IC_NO).get)
  @BeanProperty val gender = new String(myKadInfoJpn1.get(MyKadReaderScala.GENDER).get)
  @BeanProperty val dob = retrieve(myKadInfoJpn1.get(MyKadReaderScala.DOB).get)
  @BeanProperty val oldIc = new String(myKadInfoJpn1.get(MyKadReaderScala.OLD_IC).get)
  @BeanProperty val birthPlace = new String(myKadInfoJpn1.get(MyKadReaderScala.BIRTH_PLACE).get)
  @BeanProperty val nationality = new String(myKadInfoJpn1.get(MyKadReaderScala.NATIONALITY).get)
  @BeanProperty val race = new String(myKadInfoJpn1.get(MyKadReaderScala.RACE).get)
  @BeanProperty val religion = new String(myKadInfoJpn1.get(MyKadReaderScala.RELIGION).get)
  @BeanProperty val dateIssued = retrieve(myKadInfoJpn1.get(MyKadReaderScala.DATE_ISSUED).get)

  // /jpn2
  @BeanProperty val address1 = new String(myKadInfoJpn2.get(MyKadReaderScala.ADDRESS_1).get)
  @BeanProperty val address2 = new String(myKadInfoJpn2.get(MyKadReaderScala.ADDRESS_2).get)
  @BeanProperty val address3 = new String(myKadInfoJpn2.get(MyKadReaderScala.ADDRESS_3).get)
  @BeanProperty val state = new String(myKadInfoJpn2.get(MyKadReaderScala.STATE).get)
  @BeanProperty val city = new String(myKadInfoJpn2.get(MyKadReaderScala.CITY).get)
  @BeanProperty val postcode = retrieve(myKadInfoJpn2.get(MyKadReaderScala.POSTCODE).get)

  println("MyKad object created")

  def retrieve(byteArray: Array[Byte]) =
    byteArray.map( byteData => Util.toHexStr(byteData.toInt).replace("f", "")).toList.mkString

}

object MyKadData {
  def apply(myKadInfoJpn1: Map[String, Array[Byte]],
            MyKadInfoJpn2: Map[String, Array[Byte]],
            photoByteArray: ByteArrayOutputStream  ) = new MyKadData(myKadInfoJpn1, MyKadInfoJpn2, photoByteArray)
}
