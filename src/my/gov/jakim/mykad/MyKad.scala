package my.gov.jakim.mykad

import scala.swing._
import scala.swing.event.ButtonClicked
import netscape.javascript.JSObject


/**
 * Created with IntelliJ IDEA.
 * User: Mohd Solehuddin Abd Wahab
 * Date: 6/29/13
 * Time: 9:30 PM
 */
class MyKad extends Applet {

  val reader = new MyKadReaderScala
  var myKadData: MyKadData = null
  var photoDir = ""
  var biometricFlagDir = ""
  var biometricExe = ""

  object ui extends UI {
    val mainPanel = new FlowPanel() {
      //val mainPanel = new BorderPanel() {

      val readBtn = new Button("Baca Maklumat MyKad")

      contents += readBtn

      listenTo(readBtn)
      reactions += {
        case ButtonClicked(b) => {
          println("calling folder watcher..")
          if (fileReadActor.getState.toString == "Terminated") fileReadActor.restart() else fileReadActor.start()
          fileReadActor ! "start"
        }
      }
    }
    contents = mainPanel

    def init(): Unit = {
      photoDir = getParameter("photoDir")
      biometricFlagDir = getParameter("biometricDir")
      biometricExe = getParameter("biometricExe")
    }

    val fileReadActor = new SwingWorker {
      def act() {
        receive {
          case "start" => {
            myKadData = FolderWatch.watchdir(reader, biometricExe, biometricFlagDir).getOrElse(null)
            if (myKadData != null) {
              println("fishy!!!")
              Util.savePhotoToDisk(myKadData.photoByteArray, photoDir, myKadData.icNo)
              sendToPageJs
            }
            exit()
          }
        }
      }
    }

  }

  def sendToPageJs = {
    val jsobj = JSObject.getWindow(this)

    if (myKadData.gender.equalsIgnoreCase("L")) {
      //for male
      jsobj.setMember("namaL", myKadData.name)
      jsobj.setMember("noKp", myKadData.icNo)
      jsobj.setMember("religion", myKadData.religion)
      jsobj.setMember("dobL", myKadData.dob)
      jsobj.setMember("pobL", myKadData.birthPlace)
      jsobj.setMember("bangsaL", myKadData.race)
      jsobj.setMember("gender", myKadData.gender)
      jsobj.setMember("wargaL", myKadData.nationality)
      jsobj.setMember("noKpLama", myKadData.oldIc)
      jsobj.setMember("addL1",myKadData.address1)
      jsobj.setMember("addL2",myKadData.address2)
      jsobj.setMember("addL3",myKadData.address3)
      jsobj.setMember("postcodeL",myKadData.postcode)
      jsobj.setMember("townL",myKadData.city)
      jsobj.setMember("stateL",myKadData.state)
      jsobj.call("populateL()", null)
    } else {
      //for female
      jsobj.setMember("namaP", myKadData.name)
      jsobj.setMember("noKpP", myKadData.icNo)
      jsobj.setMember("religion", myKadData.religion)
      jsobj.setMember("dobP", myKadData.dob)
      jsobj.setMember("pobP", myKadData.birthPlace)
      jsobj.setMember("bangsaP", myKadData.race)
      jsobj.setMember("gender", myKadData.gender)
      jsobj.setMember("wargP", myKadData.nationality)
      jsobj.setMember("noKpLamaP", myKadData.oldIc)
      jsobj.setMember("addP1",myKadData.address1)
      jsobj.setMember("addP2",myKadData.address2)
      jsobj.setMember("addP3",myKadData.address3)
      jsobj.setMember("postcodeP",myKadData.postcode)
      jsobj.setMember("townP",myKadData.city)
      jsobj.setMember("stateP",myKadData.state)
      jsobj.call("populateP()", null)
    }

  }
}

