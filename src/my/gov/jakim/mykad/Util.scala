package my.gov.jakim.mykad

import java.io.{File, FileOutputStream, ByteArrayOutputStream}

/**
 * Created with IntelliJ IDEA.
 * User: Mohd Solehuddin Abd Wahab
 * Date: 7/2/13
 * Time: 4:44 AM
 */
object Util {

  def toHexStr(integer: Integer):String = {
    val hexedByte = Integer.toHexString(integer)
    if(hexedByte.length < 2)
      "0" + hexedByte else hexedByte
  }

  def savePhotoToDisk(byteArrayOutputStream: ByteArrayOutputStream, photoDir:String, icNo:String) = {

    val filePath = new File(photoDir)
    filePath.mkdirs()
    val fileOutputStream = new FileOutputStream(photoDir +"\\"+ icNo.trim +".jpg")
    byteArrayOutputStream.writeTo(fileOutputStream)
    byteArrayOutputStream.close();
    fileOutputStream.close();
  }

}
